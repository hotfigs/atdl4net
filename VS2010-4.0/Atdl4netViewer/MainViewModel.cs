﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Input;
using Atdl4net.Fix;
using Atdl4net.Model.Elements;
using Atdl4net.Xml;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using System.Linq;

namespace Atdl4netViewer
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private Strategy_t strategy;

        public MainViewModel()
        {
            this.InitialFix = "35=D,40=1,59=0"; // Default to new order, market day
            this.LoadFileCommand = new RelayCommand(this.LoadFile);
            this.Validate = new RelayCommand(this.ValidateStrategy);
            this.InitialiseFixCommand = new RelayCommand(this.InitialiseFix);
        }

        public void SetError(string error)
        {
            this.Error = error;
            this.OnPropertyChanged("Error");
        }

        private void InitialiseFix()
        {
            this.InitialFixValues = new FixTagValuesCollection(this.InitialFix.Replace(',', '\x01'));
            this.OnPropertyChanged("InitialFixValues");
        }

        private void ValidateStrategy()
        {
            if (this.DoValidation != null) this.DoValidation();
        }

        public event Action<Strategies_t> LoadedAtdl;
        public event Action DoValidation;

        private void LoadFile()
        {
            try
            {
                var dialog = new OpenFileDialog();
                var result = dialog.ShowDialog();
                if (result.Value)
                {
                    var reader = new StrategiesReader();
                    var strategies = reader.Load(dialog.FileName);
                    this.Strategies = strategies;
                    this.Strategy = strategies.FirstOrDefault();
                    this.OnPropertyChanged("Strategies");
                    this.OnPropertyChanged("Strategy");
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(String.Format("Exception loading ATDL: {0}", error.Message));
            }
        }

        public ICommand LoadFileCommand { get; private set; }
        public ICommand Validate { get; private set; }
        public ICommand InitialiseFixCommand { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public Strategy_t Strategy
        {
            get { return this.strategy; }
            set
            {
                this.strategy = value;
                this.OnPropertyChanged("Strategy");
            }
        }
        public Strategies_t Strategies { get; set; }
        public string InitialFix { get; set; }
        public FixTagValuesCollection InitialFixValues { get; set; }
        public string Error { get; set; }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}