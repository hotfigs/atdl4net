﻿using System;
using System.Collections.Generic;
using System.Windows;
using Atdl4net.Diagnostics.Exceptions;
using Atdl4net.Wpf.ViewModel;
using System.Linq;

namespace Atdl4netViewer
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.atdlControl.ExceptionOccurred += (sender, args) =>
            {
                var exception = args.ExceptionObject as Exception;
                if (exception != null) MessageBox.Show(exception.Message);
            };
            var viewModel = new MainViewModel();
            viewModel.DoValidation += () =>
            {
                try
                {
                    bool result = this.atdlControl.IsValid;
                    MessageBox.Show(string.Format("Atdl is valid: {0}", result));
                    try
                    {
                        this.atdlControl.RefreshOutputValues();

                    }
                    catch (ValidationException error)
                    {
                        MessageBox.Show(error.Message);
                    }
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            };
            this.atdlControl.ValidationStateChanged += (sender, args) => this.CheckForErrors();
            this.DataContext = viewModel;
        }

        private string FirstStrategyEditError()
        {
            if (!this.atdlControl.ViewModel.EvaluateAllStrategyEdits(this.atdlControl))
            {
                return this.atdlControl.Strategy
                           .StrategyEdits
                           .Where(strategyEdit => !strategyEdit.CurrentState)
                           .Select(strategyEdit => strategyEdit.ErrorMessage)
                           .FirstOrDefault();
            }

            return null;
        }

        private void CheckForErrors()
        {
            var error = this.FirstStrategyEditError();

            List<string> errors = this.atdlControl.ViewModel.Controls
                                      .Where(c => !c.IsValid)
                                      .Select(GetErrorMessage)
                                      .Where(c => c != null)
                                      .ToList();
            if (errors.Count > 0)
            {
                this.errorText.Text = errors.FirstOrDefault();
            }
            else
            {
                this.errorText.Text = "None";
            }
        }

        private static string GetErrorMessage(ControlViewModel vm)
        {
            if (!string.IsNullOrEmpty(vm.UnderlyingControl.Label))
            {
                return vm.UnderlyingControl.Label + ": " + vm.ToolTip;
            }
            return vm.ToolTip;
        }
    }
}
