require 'rubygems'
require 'rake'
require 'albacore'

namespace :albacore do
	desc "Build solution with MSBuild"
	msbuild :msbuild do |msb|
		msb.targets [:Build]
		msb.properties :configuration =>  :Release
		msb.solution = "VS2010-4.0\\Atdl4net.VS2010-4.0.sln"
		msb.verbosity = "d"
	end
	
	desc "create nuspec file"
	nuspec :create_nuspec => :msbuild do |nuspec|
		nuspec.id="Atdl4net-flex"
		nuspec.version = "1.1.4." + ENV['BUILD_NUMBER']
		nuspec.authors = "Steve Wilkinson, FlexTrade Spark team"
		nuspec.owners = "FlexTrade Spark team"
		nuspec.description = 
		"Atdl4net is an open source implementation of FIXatdl, the "\
		"FIX Protocol Algorithmic Trading Definition Language "\
		"standard developed by FIX Protocol Limited (FPL). This is "\
		"the FlexTrade brach."
		nuspec.title = "Atdl4net - FlexTrade branch"
		nuspec.language = "en-GB"
		nuspec.projectUrl = "http://scm1/?url=Atdl4net/"
		nuspec.output_file = "Atdl4net.nuspec"
		nuspec.file "Atdl4net\\obj\\Release\\Atdl4net.dll", "lib\\net40"
		nuspec.file "Atdl4net\\obj\\Release\\Atdl4net.pdb", "lib\\net40"
        nuspec.dependency "Common.Logging", "2.0.0"
        nuspec.dependency "Common.Logging.NLog", "2.0.0"
	end

	    desc "Create Artifacts directory"
	    task :create_artifacts_directory do
	    	if not File.directory? 'Artifacts' then
			FileUtils.mkdir('Artifacts')
		end
	    end

	desc "create the nuget package"
	nugetpack :create_nuget => [:create_artifacts_directory, :create_nuspec] do |nuget|
		nuget.nuspec      = "Atdl4net.nuspec"
		nuget.output      = "Artifacts/"
	end
end
